<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     vst <lvson1087@gmail.com>
 * @copyright  2019 @ Bizappco
 * @license    bản quyền mã nguồn mở GNU phiên bản 2
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Booking controller class.
 *
 * @since  1.6
 */
class BookControllerBooking extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'bookings';
		parent::__construct();
	}
}
