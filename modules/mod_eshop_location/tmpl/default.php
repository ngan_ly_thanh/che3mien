<?php
/**
 * @version        1.0.0
 * @package        Joomla
 * @subpackage    EShop
 * @author    Giang Dinh Truong
 * @copyright    Copyright (C) 2011 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;
$session = JFactory::getSession();
$location = $session->get('location', 50);
$text = $location == 50 ? 'Tỉnh Thái Nguyên' : 'Các khu vực khác';
?>
<div class="customer_location">
    <span class="location-text"><i class="fa fa-map-marker" aria-hidden="true"></i> Khu vực của bạn</span>
    <div class="dropdown d-inline-block">
        <button class="dropdown-toggle" type="button" data-toggle="dropdown"><?=$text ?>
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><a href="<?= modEshopLocationHelper::getUrl('50')?>">Tỉnh Thái Nguyên</a></li>
            <li><a href="<?= modEshopLocationHelper::getUrl('-1')?>">Các khu vực khác</a></li>
        </ul>
    </div>
</div>
