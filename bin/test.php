<?php
// Load XML with SimpleXml from string
$xml = '<?xml version = "1.0" encoding="Windows-1252" standalone="yes"?>
<VFPDataSet>
	<xsd:schema id="VFPDataSet" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
		<xsd:element name="VFPDataSet" msdata:IsDataSet="true">
			<xsd:complexType>
				<xsd:choice maxOccurs="unbounded">
					<xsd:element name="ORDER" minOccurs="0" maxOccurs="unbounded">
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name="tenkhach">
									<xsd:simpleType>
										<xsd:restriction base="xsd:string">
											<xsd:maxLength value="254"/>
										</xsd:restriction>
									</xsd:simpleType>
								</xsd:element>
								<xsd:element name="dienthoai">
									<xsd:simpleType>
										<xsd:restriction base="xsd:string">
											<xsd:maxLength value="20"/>
										</xsd:restriction>
									</xsd:simpleType>
								</xsd:element>
								<xsd:element name="diachi">
									<xsd:simpleType>
										<xsd:restriction base="xsd:string">
											<xsd:maxLength value="254"/>
										</xsd:restriction>
									</xsd:simpleType>
								</xsd:element>
								<xsd:element name="mahang">
									<xsd:simpleType>
										<xsd:restriction base="xsd:string">
											<xsd:maxLength value="20"/>
										</xsd:restriction>
									</xsd:simpleType>
								</xsd:element>
								<xsd:element name="soluong">
									<xsd:simpleType>
										<xsd:restriction base="xsd:decimal">
											<xsd:totalDigits value="9"/>
											<xsd:fractionDigits value="3"/>
										</xsd:restriction>
									</xsd:simpleType>
								</xsd:element>
							</xsd:sequence>
						</xsd:complexType>
					</xsd:element>
				</xsd:choice>
				<xsd:anyAttribute namespace="http://www.w3.org/XML/1998/namespace" processContents="lax"/>
			</xsd:complexType>
		</xsd:element>
	</xsd:schema>
	%s
</VFPDataSet>';

$orderRow = '<ORDER>
		<tenkhach>Trần Tiến Hưng</tenkhach>
		<dienthoai>0913031120</dienthoai>
		<diachi>CT1, Văn Khê, Tố Hữu, Hà Đông, Hà Nội</diachi>
		<mahang>123456789012</mahang>
		<soluong>12.123</soluong>
	</ORDER>
	';
$order = '';
for($i = 0; $i < 5; $i++){
    $order .= $orderRow;
}
$save_xml = file_put_contents( 'text.xml', sprintf($xml, $order) );

// Saving the whole modified XML to a new filename

