<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     vst <lvson1087@gmail.com>
 * @copyright  2019 @ Bizappco
 * @license    bản quyền mã nguồn mở GNU phiên bản 2
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Bookings list controller class.
 *
 * @since  1.6
 */
class BookControllerBookings extends BookController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Bookings', $prefix = 'BookModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
