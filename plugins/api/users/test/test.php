<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 9:51 AM
 */

use api\model\services\Onesignal;
use api\model\SUtil;


defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . '/components/com_eshop/helpers/helper.php');

class UsersApiResourceTest extends ApiResource
{
    /**
     * @OA\Post(
     *     path="/api/users/changepassword",
     *     tags={"User"},
     *     summary="Change password user",
     *     description="Change password user",
     *     operationId="post",
     *     security = { { "bearerAuth": {} } },
     *     @OA\RequestBody(
     *         required=true,
     *         description="Change password",
     *         @OA\JsonContent(ref="#/components/schemas/ChangePasswordForm"),
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(ref="#/components/schemas/ChangePasswordForm"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful login",
     *         @OA\Schema(ref="#/components/schemas/ErrorModel"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid request",
     *     )
     * )
     */
    public function get()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        //$sender = new Onesignal();
        //$sender->sendMessage();
        EshopHelper::makeOrderXml(187);
        //Update address long lat

        /*$db = JFactory::getDbo();
        $sql = 'SELECT * FROM #__eshop_addresses WHERE (lng is null or lat is null) AND country_id is not null ';
        $address = $db->setQuery($sql)->loadObjectList();
        if($address){
            foreach ($address as $add){
                $fullAddress = SUtil::getFullAddress($add->id);
                $longlatInfo = SUtil::getLonLatFromAddress($fullAddress);
                $add->lat = $longlatInfo['lat'];
                $add->lng = $longlatInfo['lng'];
                $result = $db->updateObject('#__eshop_addresses', $add, 'id');
            }
        }*/

    }

    

}
