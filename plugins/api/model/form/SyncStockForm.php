<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 8:01 AM
 */

namespace api\model\form;

use api\model\AbtractForm;

/**
 * @OA\Schema(required={"product_sku", "product_price"}, @OA\Xml(name="SyncPriceForm"))
 */
class SyncStockForm extends AbtractForm
{
    /**
     * @OA\Property(example="product_sku")
     * @var string
     */
    public $product_sku;

    /**
     * @OA\Property(example="product_quantity")
     * @var int
     */
    public $product_quantity;

    public function rule()
    {
        return array(
            'required' => array(
                'product_sku',
                'product_quantity'
            )
        );
    }

}