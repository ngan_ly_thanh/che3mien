<?php
/**
 * Created by PhpStorm.
 * User: lvson
 * Date: 4/26/2019
 * Time: 10:53 AM
 */

namespace api\model\dao;

use api\model\AbtractDao;
use api\model\biz\UserBiz;

class UserDao extends AbtractDao
{
    public $select = array(
        'id',
        'name',
        'username',
        'email'
    );

    public function getTable()
    {
        return '#__users';
    }

    public function getUsers($params = array())
    {
        $paramsDefault = array(
            'select' => $this->select,
            'where' => array(
                'block = 0'
            )
        );
        if (isset($params['where']) && $params['where']) {
            foreach ($params['where'] as $item) {
                $paramsDefault['where'][] = $item;
            }
        }
        if ($params) {
            foreach ($params as $k => $item) {
                if ($k === 'where') {
                    continue;
                }
                $paramsDefault[$k] = $item;
            }
        }
        $result = $this->get($paramsDefault);

        if ($result) {
            $biz = new UserBiz();
            $biz->setAttributes($result);
            return $biz;
        }
        return null;
    }

    public function getUserInfo($userName)
    {
        $paramsDefault = array(
            'select' => $this->select,
            'where' => array(
                'block = 0',
                'username = ' . $this->db->quote($userName)
            )
        );
        $result = $this->get($paramsDefault);

        if ($result) {
            $biz = new UserBiz();
            $biz->setAttributes($result);
            return $biz;
        }
        return null;
    }
    public function loadUser($id){
        $sql = 'SELECT * FROM '.$this->getTable(). ' WHERE id = '.$id;
        return $this->db->setQuery($sql)->loadObject();
    }

}
