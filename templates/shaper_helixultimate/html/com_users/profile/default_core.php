<?php
/**
 * @package Helix Ultimate Framework
 * @author JoomShaper https://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2018 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
*/

defined ('_JEXEC') or die();
?>

<div id="users-profile-core" class="col-md-12">
    <div class="row box_profile">
        <div class="col-md-2">
            <i class="fa fa-4x fa-user-circle"></i>
        </div>
        <div class="col-md-10">
            <p>
                <strong><?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?></strong>:
                <?php echo $this->data->name; ?>
            </p>
            <p>
                <strong><?php echo JText::_('COM_USERS_PROFILE_USERNAME_LABEL'); ?></strong>:
                <?php echo htmlspecialchars($this->data->username, ENT_COMPAT, 'UTF-8'); ?>
            </p>
            <p>

            </p>
        </div>
    </div>
    <div class="row box_profile_info">
        <div class="col-md-12">
            <h3 class="title_info"><i class="fa fa-info-circle"></i> <?php echo JText::_('COM_USERS_PROFILE_CORE_LEGEND'); ?></h3>
            <small><?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?></small>
            <p><strong><?php echo $this->data->name; ?></strong></p>
            <small><?php echo JText::_('COM_USERS_PROFILE_USERNAME_LABEL'); ?></small>
            <p><strong><?php echo htmlspecialchars($this->data->username, ENT_COMPAT, 'UTF-8'); ?></strong></p>
            <small><?php echo JText::_('COM_USERS_PROFILE_REGISTERED_DATE_LABEL'); ?></small>
            <p><strong><?php echo JHtml::_('date', $this->data->registerDate); ?></strong></p>
            <small><?php echo JText::_('COM_USERS_PROFILE_LAST_VISITED_DATE_LABEL'); ?></small>:
            <p>
                <strong>
                    <?php if ($this->data->lastvisitDate != $this->db->getNullDate()): ?>
                        <?php echo JHtml::_('date', $this->data->lastvisitDate); ?>
                    <?php else: ?>
                        <?php echo JText::_('COM_USERS_PROFILE_NEVER_VISITED'); ?>
                    <?php endif;?>
                </strong>
            </p>
        </div>
    </div>
    <div class="btn_footer">
        <?php if (JFactory::getUser()->id == $this->data->id): ?>
            <a class="btn btn-primary" title="<?php echo JText::_('COM_USERS_EDIT_PROFILE'); ?>" href="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id=' . (int) $this->data->id); ?>">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php echo JText::_('COM_USERS_EDIT_PROFILE'); ?>
            </a>
        <?php endif;?>
    </div>
</div>
